from urllib.request import urlopen
from bs4 import BeautifulSoup
from datetime import datetime


def get_sudadera_price():
    url = "https://www.pccomponentes.com/samsung-ue82mu7005-82-led-ultrahd-4k"

    # Cargamos la web anterior y se la pasamos a Beautiful Soup para que la gestione. #
    soup = BeautifulSoup(urlopen(url))

    baseprice = soup.find('div', '#baseprice').strong.contents[0]
    baseprice = baseprice[:-1]
    return baseprice


def open_file():
    csvfile = open('tv4kprecio.csv', 'a')
    return csvfile


def save_file(csvfile, current):
    now = datetime.now()
    csvfile.write(str(now.day))
    csvfile.write(str(now.month))
    csvfile.write(str(now.year))
    csvfile.write(str(now.hour))
    csvfile.write(str(now.minute))
    csvfile.write(str(now.current))
    csvfile.close()

    csvfile = open_file()
    priceblock_ourprice = get_sudadera_price()
    save_file(csvfile, priceblock_ourprice)
    print(datetime.now(), " - ok")
