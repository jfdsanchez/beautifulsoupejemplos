# Importamos las librerias
from bs4 import BeautifulSoup
import requests
import time
import os

while True:
    # Capturamos la url
    url = "https://www.pccomponentes.com/samsung-ue82mu7005-82-led-ultrahd-4k"

    # Capturamos el html de la pagina web y creamos un objeto Response
    r = requests.get(url)
    data = r.text

    # Creamos el objeto soup y le pasamos lo capturado con request
    soup = BeautifulSoup(data, 'lxml')

    # Buscamos el div para sacar los grados
    sin_iva = soup.find_all('div', class_="no-iva-base")

    # Buscamos el div para sacar la sensacion termica
    con_iva = soup.find_all('div', class_="baseprice")

    # Con [0] saco el precio sin iva y con [1] el precio con el iva incluido.
    print
    "El precio sin iva es: " + sin_iva[0].text
    print
    "El precio con iva es: " + con_iva[1].text

    # Tiempo en segundos para ejecutarse nuevamente
    time.sleep(15)

    # Borramos los datos viejos, para Windows es "cls"
    os.system("clear")